//Класс, реализирующий интерфейс Rannable
public class Thread1 implements Runnable
{
	public Thread thr; 
	Num ThrNum;
	Thread1(Num num)
	{
		thr = new Thread(this, "Dec");
		ThrNum = num;
	}
	
	public void start()
	{
		thr.start();
	}
	
	public void run()
	{
		while(true)
		{
			synchronized(ThrNum)
			{
				if(ThrNum.globalCount != -1)
				{
					for(int i=0; i<ThrNum.count; i++)
						System.out.println(thr.getName()+": "+(i+1));
					ThrNum.notify();
					try {ThrNum.wait();}
					catch(Exception err){}
				}
				else return;
			}
			try {wait();}
			catch(Exception err){}
		}
	}
}

