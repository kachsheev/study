//класс, реализующий наследование от Thread
public class Thread2 extends Thread
{
	Num ThrNum;
	Thread2(Num num)
	{
		super("Gen");
		ThrNum = num;
	}
	
	public void run()
	{
		while(true)
		{
			synchronized(ThrNum)
			{
				if(ThrNum.globalCount != 0)
				{
					ThrNum.gen();
					System.out.println(getName()+":  "+ThrNum.count+" "+ThrNum.globalCount);
					ThrNum.notify();
					try {ThrNum.wait();}
					catch(Exception err){}
				}
				else
				{
					//ThrNum.notify();
					return;
				}
			}
		}

	}
}
