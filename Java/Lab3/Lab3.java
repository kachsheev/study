
public class Lab3 {

	public static void main(String[] args) {
		A objA = new A();
		B objB = new B();
		
		String strA = "objA" ,strB = "objB";
		objA.a(strA);
		objA.i1(strA);
		
		objB.b(strB);
		objB.i2(strB);
		objB.a(strB);
		objB.i1(strB);
		
		I1 objB1 = objB;
		I1 objA1 = objA;
		
		objA1.i1(strA+"1");
		objB1.i1(strB+"1");
	}

}
