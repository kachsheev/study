/*3. ������� ���������� � 2 �������� - �, B  � 2 ������������ I1, I2.
����� � �������� ������������ ��� ������ B.
��������� I1 �������� ������������ ��� ���������� I2.
����� A ��������� ��������� I1.
����� � ��������� ��������� I2.
��������� I1 �������� ����� i1, ����� � �������� ����� �1, ��������� I2 �������� ����� i2. 
����� B �������� ����� b1.
��� ������ ������� ������ � ������ ������ ������ ��� ���������� � ������ ������.
�������  2 ������� ����  A, B . ��������� ������������ ����� ������� ��� ������� �������.
�������� ������� � ���� I1 � ����� ��������� ������������ ����� ������� ��� ���� ��������.*/
public class Lab3 {

	public static void main(String[] args) {
		A objA = new A();
		B objB = new B();
		
		String strA = "objA" ,strB = "objB";
		objA.a(strA);
		objA.i1(strA);
		
		objB.b(strB);
		objB.i2(strB);
		objB.a(strB);
		objB.i1(strB);
		
		I1 objB1 = objB;
		I1 objA1 = objA;
		
		objA1.i1(strA+"1");
		objB1.i1(strB+"1");
	}

}