

public class Lab6 {
	public static void main(String[] args) {
		try
		{
			int coutOfThreads = 0;
			NumberBox Number;

			coutOfThreads = Integer.parseInt(args[0]);
			Number = new NumberBox(Integer.parseInt(args[1]));
		
			MyThread Threads[] = new MyThread[coutOfThreads];
			Object Monitors[] = new Object[coutOfThreads];
			
			for(int i=0; i<coutOfThreads; i++)
			{
				Monitors[i] = new Object();
			}
			
			for(int i=0; i<(coutOfThreads-1); i++)
			{
				Threads[i] = new MyThread("Thrd_"+(i+1), Monitors[i], Monitors[i+1], Number);
			}
			
			Threads[coutOfThreads-1] = new MyThread("Thrd_"+(coutOfThreads), Monitors[coutOfThreads-1], Monitors[0], Number);
			
			for(int i=0; i<coutOfThreads; i++)
			{
				Threads[i].start();
			}
			synchronized(Monitors[0])
			{
				Monitors[0].notify();
			}
		}
		catch(ArrayIndexOutOfBoundsException err)
		{
			System.out.println("Error: arguments did not enter");
			return;
		}
		catch(NumberFormatException err)
		{
			System.out.println("Error: invalid format");
			return;
		}
		catch(NegativeArraySizeException err)
		{
			System.out.println("Error: invalid format");
			return;
		}
	}

}
