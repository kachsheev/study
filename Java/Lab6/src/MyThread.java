
public class MyThread extends Thread {
	Object MyMonitor, NextMonitor;
	NumberBox Number;
	
	MyThread(String name, Object myMonitor, Object nextMonitor, NumberBox number)
	{
		super(name);
		MyMonitor = myMonitor;
		NextMonitor = nextMonitor;
		Number = number;
	}
	
	public void run()
	{
		while(true)
		{
			synchronized(MyMonitor)
			{
				try{//System.out.println(getName()+": waiting my monitor...");
				MyMonitor.wait();}
				catch(Exception err){}
				if(Number.count>0)
				{
					Number.count--;
					System.out.println(getName()+": "+Number.count);
					synchronized(NextMonitor)
					{	NextMonitor.notify();}
				}
				else
				{
					synchronized(NextMonitor)
					{	NextMonitor.notify();}
					return;
				}
			}
		}
	}
}
