import java.io.*;

public class MyFileFilter implements FileFilter //класс для фильтрации содержимого директории
{
	public boolean accept(File arg)
	{
		return arg.isFile();
	}
}
