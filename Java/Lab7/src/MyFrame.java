import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;

public class MyFrame extends JFrame implements ActionListener
{
	private static final long serialVersionUID = 1L;

	JLabel thisLabel_EnterDir, thisLabel_EnterOutFile, thisLabel_FindString, thisLabel_Warning;
	JButton thisButton_Start, thisButton_Exit;
	JTextField thisTextField_EnterDir, thisTextField_EnterOutFile, thisTextField_FindString;
	LayoutManager MyLM;
	
	MyFrame()
	{
		this.initComponents();
		this.setSettings();
	}
	// инициальзация компонентов
	void initComponents()
	{
		thisLabel_EnterDir =     new JLabel("                  Enter a directory: ");
		MyLM = new FlowLayout(); //GridBagLayout //можно использовать этот
		thisLabel_EnterOutFile = new JLabel("      Enter a name of out file: ");
		thisLabel_FindString =   new JLabel("Find string in the directory: ");
		thisTextField_EnterDir = new JTextField(24);
		thisTextField_EnterOutFile = new JTextField(24);
		thisTextField_FindString = new JTextField(24);
		thisButton_Start = new JButton("Start");
		thisButton_Exit = new JButton("Exit");
		thisLabel_Warning = new JLabel("");
	}
	// установка настроек
	public void setSettings()
	{
		setTitle("Lab7");
		setLayout(MyLM);
		add(thisLabel_EnterDir);
		add(thisTextField_EnterDir);
		add(thisLabel_EnterOutFile);
		add(thisTextField_EnterOutFile);
		add(thisLabel_FindString);
		add(thisTextField_FindString);
		add(thisButton_Start);
		add(thisButton_Exit);
		add(thisLabel_Warning);
		
		setDefaultCloseOperation(EXIT_ON_CLOSE); // для отображения кнопок сворачивания и закрытия
		setSize(540, 180);
		
		thisButton_Start.addActionListener(this);
		thisButton_Exit.addActionListener(this);
	}
	//обработчик событий
	public void actionPerformed(ActionEvent ae) 
	{
		//нажатие кнопки Start
		if(ae.getActionCommand().equals("Start"))
		{
			start();
		}
		else // нажатие кнопки Exit
		if(ae.getActionCommand().equals("Exit"))
		{
			dispose();
		}
	}
	// проверка заполнения полей
	boolean isEmptyTextFields()
	{
		if(this.thisTextField_EnterDir.getText().isEmpty() || 
				this.thisTextField_EnterOutFile.getText().isEmpty()
				|| this.thisTextField_FindString.getText().isEmpty())
			return true;
		else
			return false;
	}
	
	@SuppressWarnings("resource")
	void start()
	{
		/* 	
		 	тут должна быть вся основа
			а) получение строк из текстовых полей
			б) получение списка файлов из введенной директории
				- если нет такой директории, то ошибка
			в) открытие каждого файла для нахождения строки
		*/
		if(this.isEmptyTextFields()) //проверка на наличие пустых текстовых полей
		{
			this.thisLabel_Warning.setText("Fill all text fields!");
		}
		else
		{
			File thisDir = new File(this.thisTextField_EnterDir.getText()); //проверка на наличие директории
			if(thisDir.exists())
			{
				System.out.println("Entered path is exist");
				this.thisLabel_Warning.setText("");
				
				// получили список файлов в директории
				MyFileFilter filter = new MyFileFilter();
				File inDirFiles[] = thisDir.listFiles(filter); 
				
				// создаю файл в той же директории
				File myFile = new File(/*thisDir,*/thisTextField_EnterOutFile.getText()); 
				try 
				{
					if(!myFile.createNewFile()) 
					{
						System.out.println("File exist");
					}
				}
				catch(IOException ioerr)
				{
					System.out.println("IOException");
					return;
				}
				
				// объявляю потоки ввода и вывода для работы с файлами
				FileInputStream input = null;
				FileOutputStream output = null;
				try
				{
					output = new FileOutputStream(this.thisTextField_EnterOutFile.getText());
					//System.out.println(this.thisTextField_EnterOutFile.getText());
					System.out.println("Output Stream open file");
				}
				catch(Exception err)
				{
					this.thisLabel_Warning.setText("File not found!");
					return;
				}


				//Вывод всех найденных файлов в каталоге
				for(int i=0; i<inDirFiles.length; i++) System.out.println(inDirFiles[i]);
				
				String find = this.thisTextField_FindString.getText();
				String str = new String("");
				int ich, index = 0;
				for(int i=0;i<inDirFiles.length; i++)
				{
					// открыли файл
					try
					{
						input = new FileInputStream(inDirFiles[i].toString());
					}
					catch (FileNotFoundException err)
					{
						System.out.println("File not found");
						return;
					}
					
					//чтение и сравнение
					try {
						
						ich = input.read();
						while(ich != -1)
						{
							if( (char)ich == find.charAt(index))
								index++;
							else
								index=0;
							
							if(index == find.length())
							{
								index = 0;
								str = "Find: "+ inDirFiles[i].toString()+"\n";
								System.out.println(str);
								output.write(str.getBytes());
							}
							
							ich = input.read();
						}
						
					}catch(IOException err){System.out.println("Error reading file");}
					//
					
					
					
					// закрыли файл
					try
					{
						input.close();
					}
					catch(IOException err)
					{
						System.out.println("Error closing file");
					}
				}
				
				try
				{
					output.close();
					System.out.println("Output Stream close file");
				}
				catch(IOException err)
				{
					System.out.println("Error closing file");
				}

			}
			else
			{
				this.thisLabel_Warning.setText("Entered path is not exist!");
			}
			
			
		}
		
	}

};