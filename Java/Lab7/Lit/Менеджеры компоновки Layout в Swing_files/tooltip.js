function createTooltips() {
    var Box = function(o) {
        function winWidth() {
            return winFilterResult (
                    window.innerWidth ? window.innerWidth : 0,
                    document.documentElement ? document.documentElement.clientWidth : 0,
                    document.body ? document.body.clientWidth : 0
                    );
        }
        function winHeight() {
            return winFilterResult (
                    window.innerHeight ? window.innerHeight : 0,
                    document.documentElement ? document.documentElement.clientHeight : 0,
                    document.body ? document.body.clientHeight : 0
                    );
        }
        function winLeft() {
            return winFilterResult (
                    window.pageXOffset ? window.pageXOffset : 0,
                    document.documentElement ? document.documentElement.scrollLeft : 0,
                    document.body ? document.body.scrollLeft : 0
                    );
        }
        function winTop() {
            return winFilterResult (
                    window.pageYOffset ? window.pageYOffset : 0,
                    document.documentElement ? document.documentElement.scrollTop : 0,
                    document.body ? document.body.scrollTop : 0
                    );
        }
        function winFilterResult(n_win, n_docel, n_body) {
            var n_result = n_win ? n_win : 0;
            if (n_docel && (!n_result || (n_result > n_docel)))
                n_result = n_docel;
            return n_body && (!n_result || (n_result > n_body)) ? n_body : n_result;
        }

        function position(el) {
            var left = 0, top = 0;
            do {
                left += el.offsetLeft || 0;
                top += el.offsetTop || 0;
                el = el.offsetParent;
            } while (el);
            return {'x': left, 'y': top};
        }


        var box = {};
        if (o.location) { //window
            box.top = winTop(); box.left = winLeft();
            box.bottom = winHeight() + box.top; box.right = winWidth() + box.left;
            box.width = box.right - box.left;
            box.height = box.bottom - box.top;
        } else { //element
            var p = position(o);
            box.top = p.y; box.left = p.x;
            box.width = o.offsetWidth; box.height = o.offsetHeight;
            box.bottom = box.top + box.height; box.right = box.left + box.width;
        }

        return box;
    };

    var Layer = function(element, options) {
        this.element = element;
        this.options = {
            zIndex: 10000,
            iframeUrl: null
        };
        if (options) {
            if (options.zIndex) this.options.zIndex = options.zIndex;
            if (options.iframeUrl) this.options.iframeUrl = options.iframeUrl;
        }
        this.element.style['visibility']='hidden';
        this.element.style['position']='absolute';
        this.element.style['top']='-300px';
        this.element.style['left']='-3000px';
        this.element.style['zIndex']=this.options.zIndex;

        this.overlaps = function(el) {
            var thisCoord = new Box(this.element);
            var elCoord = new Box(el);

            var overlapsX =
                    (thisCoord.left >= elCoord.left && thisCoord.left <= elCoord.right) ||
                    (thisCoord.right >= elCoord.left && thisCoord.right <= elCoord.right) ||
                    (elCoord.left >= thisCoord.left && elCoord.left <= thisCoord.right);

            var overlapsY =
                    (thisCoord.top >= elCoord.top && thisCoord.top <= elCoord.bottom) ||
                    (thisCoord.bottom >= elCoord.top && thisCoord.bottom <= elCoord.bottom) ||
                    (elCoord.top >= thisCoord.top && elCoord.top <= thisCoord.bottom);

            return overlapsX && overlapsY;
        };

        this.fits = function(o) {
            var box = new Box(o);

            var c = new Box(this.element);
            if (c.left < box.left || c.left + c.width > box.right) return false;
            return !(c.top < box.top || c.top + c.height > box.bottom);

        };

        this.moveTo = function(x, y) {
            var p = this.element.offsetParent ? new Box(this.element.offsetParent) : {left:0, top:0};
            this.element.style['left'] = (x - p.left) + "px";
            this.element.style['top'] = (y - p.top) + "px";
        };

        this.alignBy = function(o, options) {
            if (!options) options = {};
            if (!options.position) options.position = "top left";
            if (options.inside==null) options.inside = false;
            if (!options.offsets) options.offsets = {x:0, y:0};

            var box = new Box(o);
            var elBox = new Box(this.element);
            var x = box.left, y = box.top;


            if ( options.inside ) {
                if (options.position.indexOf("center") != -1) x = (box.width - elBox.width) / 2 + box.left;
                if (options.position.indexOf("bottom") != -1) y = box.top + box.height - elBox.height - options.offsets.y;
                if (options.position.indexOf("top") != -1) y = box.top + options.offsets.y;

                if (options.position.indexOf("middle") != -1) y = (box.height - elBox.height) / 2 + box.top;
                if (options.position.indexOf("right") != -1) x = box.left + box.width - elBox.width - options.offsets.x;
                if (options.position.indexOf("left") != -1) x = box.left + options.offsets.x;
            } else {
                if (options.position.indexOf("top") != -1) y -= elBox.height + options.offsets.y;
                if (options.position.indexOf("bottom") != -1) y += box.height + options.offsets.y;
                if (options.position.indexOf("left") != -1) x -= elBox.width + options.offsets.x;
                if (options.position.indexOf("right") != -1) x += box.width + options.offsets.x;
            }

            this.moveTo(x, y);
            this.syncIFrame();
        };

        this.syncIFrame = function() {
            if (window.ActiveXObject && this.options.iframeUrl) {
                if ( !this.iframe ) {
                    this.iframe = document.createElement("iframe");

                    this.iframe.style['visibility'] = 'hidden';
                    this.iframe.style['position'] = 'absolute';
                    this.iframe.style['left'] = '-10000px';
                    this.iframe.style['top'] = '-10000px';
                    this.iframe.style['border'] = 'none';
                    this.iframe.style['zIndex'] = this.element.style.zIndex - 1;;

                    this.iframe.src=this.options.iframeUrl;
                    this.iframe.scrolling = "no";
                    this.iframe.frameBorder = "0";

                    this.element.parentNode.insertBefore(this.iframe, this.element);
                }

                this.iframe.top= this.element.top;
                this.iframe.left = this.element.left;
            }
        };

        this.show = function() {
            this.element.style["visibility"] = "visible";
            if ( this.iframe ) this.iframe.style["visibility"] = "visible";
        };

        this.hide = function() {
            this.element.style["visibility"] = "hidden";
            if ( this.iframe ) this.iframe.style["visibility"] = "hidden";
        };

        this.isVisible = function() {
            return this.element.style["visibility"].indexOf("visible") != -1;
        };

        this.toogle = function() {
            if ( this.isVisible() ) this.hide();
            else this.show();
        };

        this.remove = function() {
            if (this.iframe) this.iframe.parentNode.removeChild(this.iframe);
            this.element.parentNode.removeChild(this.element);
        };
    };

    var createTip = function(div, activator) {
        var temp = "<table class='tip' cellspacing='0' cellpadding='0'><tbody>\
                        <tr class='tip-t'>\
                        <td class='tip-tl'><b class='tip-strut'><i class='tip-stock-tl'></i></b></td>\
                        <td class='tip-tm'></td>\
                        <td class='tip-tr'><b class='tip-strut'><i class='tip-stock-tr'></i></b></td>\
                        </tr>\
                    <tr class='tip-m'><td class='tip-ml'></td><td class='tip-mm'></td><td class='tip-mr'></td></tr>\
                        <tr class='tip-b'>\
                        <td class='tip-bl'><b class='tip-strut'><i class='tip-stock-bl'></i></b></td>\
                        <td class='tip-bm'></td>\
                        <td class='tip-br'><b class='tip-strut'><i class='tip-stock-br'></i></b></td>\
                        </tr>\
                    </tbody></table>";

        var tip = document.createElement("div");
        tip.innerHTML = temp;
        tip = tip.childNodes[0];

        var className = div.className;
        div.className = "";
        div.parentNode.appendChild(tip);

        div.parentNode.removeChild(div);

        var tipContent = tip.getElementsByTagName("tr")[1].getElementsByTagName("td")[1];
        tipContent.appendChild(div);

        var layer = new Layer(tip);
        layer.hideIfRequeired=function() {
            if ( !layer.activated && !layer.locked ) {
                layer.hide();
                layer.element.className = 'tip';
            }
        };

        tip.onmouseover = function() { layer.locked=true; };
        tip.onmouseout = function() { layer.locked=false; setTimeout(layer.hideIfRequeired, 200); };

        var activate = className == "tip" ? "onmouseover" : "onfocus";
        var deactivate = className == "tip" ? "onmouseout" : "onblur";

        activator[activate] = function() {
            layer.activated = true;
            layer.element.className="";

            //находим позицию для тултипа
            layer.alignBy(activator, {position:"right bottom", offsets:{x:0, y:-5}});
            if (layer.fits(window)) layer.element.className = "tip stock-tl";
            else {
                layer.alignBy(activator, {position:"left bottom", offsets:{x:0, y:-5}});
                if (layer.fits(window)) layer.element.className = "tip stock-tr";
                else {
                    layer.alignBy(activator, {position:"left top", offsets:{x:0, y:-5}});
                    if (layer.fits(window)) layer.element.className = "tip stock-br";
                    else {
                        layer.alignBy(activator, {position:"right top", offsets:{x:0, y:-5}});
                        layer.element.className = "tip stock-bl";
                    }
                }
            }

            layer.show();
        };
        activator[deactivate] = function() { layer.activated = false; setTimeout(layer.hideIfRequeired, 200); };
    };

    function lazilyCreateTip(div) {
        var forId = div.attributes['for'].value;
        var activator = document.getElementById(forId);
        if (activator == null) throw "Can't create tip. No activator with id " + div.getAttribute("for");

        var activate = div.className == "tip" ? "onmouseover" : "onfocus";
        activator[activate] = function() {
            createTip(div, activator);
            activator[activate]();
        };
    }

    var divs = document.getElementsByTagName("div");
    for(var i = 0; i < divs.length; i++) {
        if (divs[i].className == "tip" || divs[i].className == "baloon")
            lazilyCreateTip(divs[i]);
    }
}