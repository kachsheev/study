//FEEDBACKS

function Feedback() {
    function initialize() {
        this.form = jQuery('#feedback');
        this.form.submit(function() { return false;});

        jQuery("#feedback input[type=button]").click(function() {
            jQuery("#feedback input[type=button]").attr('disabled', 'disabled');
            jQuery.ajax({
                type:"POST",
                cache:false,
                url:"/sidebar/feedback",
                data:"text=" + jQuery('#feedback textarea').val() + "&captcha=" + jQuery('#feedback input[name=captcha]').val(),
                dataType:'json',
                success: function(errors) {
                    syncCaptcha(false);
                    setErrors(errors);
                    jQuery("#feedback input[type=button]").removeAttr('disabled');
                    if (errors.length == 0) {
                        jQuery("#feedback-block .content").prepend("<div class='thanks'><span>Cпасибо!</span><a href='#'>оставить еще</a></div>");
                        var thanks = jQuery("#feedback-block .content .thanks");
                        var moreLink = jQuery("#feedback-block .content .thanks a");
                        moreLink.click(function() {
                            thanks.remove();
                            jQuery('#feedback').slideDown();
                        });
                        jQuery('#feedback').slideUp();

                        jQuery('#feedback')[0].reset();
                    }
                },
                error: function(xhr) {
                    var status = 0;
                    try { status = xhr.status; } catch(e){};
                    if (status >= 400  && status < 600) jQuery("#feedback").html("<span style='color:red'>Ошибка " + status + "</span>");
                    else jQuery("#feedback").html("<span style='color:red'>Ошибка соединения с сервером</span>");
                }
            });
        });

        var captchaImage = jQuery(".captcha-img");
        if (captchaImage) {
            captchaImage.click(function() { syncCaptcha(true); });
            syncCaptcha(true);
        }

        if (Cookie.get("feedbackState") == "opened")
            jQuery("#feedback-block-button").removeClass('closed').addClass('opened');
    }

    function syncCaptcha(refresh) {
        var captcha = jQuery(".captcha-img");
        if (captcha) captcha.attr("src", "/captcha/feedback?" + Math.random() + (refresh ? "&refresh" : ""));
    }

    function setErrors(errors) {
        var div = jQuery("#feedback .errors");
        div.empty();

        jQuery(errors).each(function(i) {
            div.append("<div>" + this + "</div>");
        });
        if (errors.length > 0) div.removeClass("hidden");
        else div.addClass("hidden");
    }

    initialize();
}


//POLLS

function Poll() {
    this.element = jQuery("#pollContent");
    nextPoll();
    var _this = this;
    if (Cookie.get("pollState") == "closed")
          jQuery("#poll").removeClass("opened").addClass("closed");
    bindActions();

    function createUi(isResult) {
        _this.element.append("<div class='poll'><p></p><ul></ul></div>");
        _this.element.find('p').html(_this.poll.title + ((isResult) ? " (голосов: " + _this.poll.votes + ")" : ""));

        for (var i = 0 ; i < _this.poll.options.length ; i++) {
            var p = _this.poll.options[i];
            _this.element.find('ul').append('<li></li>');
            var optionEl = _this.element.find('li:last');
            if (!isResult) {
                optionEl.append("<input type='radio' id='" + p.id + "' name='" + _this.poll.id + "' value='" + p.id + "' />");
                optionEl.append("<label for='" + p.id + "' >" + p.text + "</label>");
                optionEl.find("label").click( function() { optionEl.find("input").click(); } );
            } else
                optionEl.append("<span>" + p.text + " - <b> " + p.votes + "(" + p.percent + "%)</b>" + "</span>");
        }

        if (!isResult) {
            _this.element.find('.poll').append('<p class="buttons"><input type="button" value="Ответить" /></p>');
            _this.element.find('.buttons input').click( function() {
                _this.element.find("input[type=radio]:checked").each(function() {
                        jQuery.ajax({
                            type: "GET",
                            dataType: "json",
                            url: "/sidebar/poll/result/" + _this.poll.id + "/option/" + this.id,
                            cache: false,
                            beforeSend: function() { _this.element.html("<span style='font-size:1.3em;'>Загрузка ...</span>"); },
                            success: function(text) {
                                _this.element.html("");
                                _this.poll = text;
                                createUi(true);
                            },
                            error: function(xhr) {
                                var status = 0;
                                try {status = xhr.status;} catch(e){};
                                if (status >= 400  && status < 600) _this.element.html('<span style="color:red">Ошибка ' + status + '</span>');
                                else _this.element.html('<span style="color:red">Ошибка соединения с сервером</span>');
                            }
                        });
                });
            });
        } else {
            _this.element.find('.poll').append('<p class="buttons"><input type="button" value="Далее" /></p>');
            _this.element.find('.buttons input').click( function() { nextPoll(); } );
        }
    }

    function nextPoll() {
        jQuery.ajax({
            type: "GET",
            dataType: "json",
            url: '/sidebar/poll/display',
            cache: false,
            beforeSend: function() { jQuery('#pollContent').html("<span style='font-size:1.3em;'>Загрузка ...</span>"); },
            success: function(text) {
                _this.element.html("");
                if (!text || !text.options) {
                    _this.element.addClass("hidden");
                    jQuery("#poll").fadeOut();
                    return;
                }
                _this.poll = text;
                createUi(false);
            },
            error: function(xhr) {
                var status = 0;
                try {status = xhr.status;} catch(e){};
                if (status >= 400  && status < 600) _this.element.html('<span style="color:red">Ошибка ' + status + '</span>');
                else _this.element.html('<span style="color:red">Ошибка соединения с сервером</span>');
            }
        });
    }

    function bindActions() {
        jQuery('#poll .title').click(function() {
            if (jQuery('#poll').hasClass('opened')) {
                jQuery('#poll .content').slideUp();
                jQuery('#poll').removeClass('opened').addClass('closed');
                Cookie.set("pollState", "closed", 365);
            }
            else {
                jQuery('#poll .content').slideDown();
                jQuery('#poll').removeClass('closed').addClass('opened');
                Cookie.set("pollState", "opened", 365);
            }
        });
    }

}

//TIPS

function Tips() {
    this.index;

    var _this = this;

    function changeTip(index) {
        jQuery.ajax({
            type:'GET',
            cache:false,
            url:'/sidebar/tips',
            data:'tip=' + index,
            dataType:'json',
            success:function(data) {
                jQuery('#tips-text').html(data.text);
                _this.index = data.index;
                jQuery('#tips-loading').css('display', 'none');
            },
            beforeSend:function() {
                jQuery('#tips-loading').css('display', 'inline');
            },
            error:function(xhr) {
                var status = 0;
                try { status = xhr.status; } catch(e) { };
                if (status >= 400  && status < 600) jQuery('#tips-text').parent('.content').html('<span style="color:red">Ошибка ' + status + '</span>');
                else jQuery('#tips-text').parent('.content').html("<span style='color:red'>Ошибка соединения с сервером</span>"); }
        });
    }

    function arrows() {
        jQuery("#tips-block .link span:first").click(function() {
            changeTip(_this.index - 1);
        });

        jQuery("#tips-block .link span:last").click(function() {
            changeTip(_this.index + 1);
        });
    }

    function bindActions() {
        jQuery("#tips-block .title").click(function() {
            if (jQuery('#tips-block').hasClass('closed')) {
                jQuery('#tips-block .content').slideDown();
                jQuery('#tips-block').removeClass('closed').addClass('opened');
                Cookie.set("tipState", "opened", 365);
            }
            else {
                jQuery('#tips-block .content').slideUp();
                jQuery('#tips-block').removeClass('opened').addClass('closed');
                Cookie.set("tipState", "closed", 365);
            }
        });
    }

    function checkState() {
        if (Cookie.get("tipState") == "closed")
            jQuery('#tips-block').removeClass('opened').addClass("closed");
    }

    checkState();
    bindActions();
    arrows();
    changeTip(jQuery("#tip-index").val());
}
//Top Contributors

function TopContributors() {
    this.page = 0;
    this.entries;
    this.user;
    this.lastPage;

    var _this = this;

    function createReport() {
        var table = jQuery("#contributors-table");
        table.html('');

        table.append("<table class='table'><tbody></tbody></table><a href='/page/contribute' style='float:left; margin: 3px 0 0 5px;'>Получение Q</a>" );

        var tbody = table.find('tbody');

        for (var i = 0; i < _this.entries.length; i++ ) {
            tbody.append('<tr></tr>');

            var row = tbody.find('tr:last');

            row.append('<td class="place">' + (_this.page * 10 + i + 1) + '.</td>');

            var name = _this.entries[i].name;
            row.append("<td class='login'>" + name + "</td>");

            var points = _this.entries[i].points / 10.0 + (_this.entries[i].points % 10 == 0 ? ".0" : "") + "&nbsp;Q";
            row.append("<td class='points'>" + points + "</td>");

            if (_this.user == name) row.css('font-weight', 'bold');
        }

        jQuery('#contributors-table .table tr:even').toggleClass('second');
        jQuery('#contributors-table .table tr:odd').toggleClass('first');

        var leftArrow = jQuery("#contribution-paging span:first");
        var rightArrow = jQuery("#contribution-paging span:last");

        if (_this.page == 0)
            leftArrow.css({"color": "gray", "cursor": "default"});
        else
            leftArrow.css({"color" : "#419FDD", "cursor": "pointer"});

        if (_this.lastPage)
            rightArrow.css({"color": "gray", "cursor": "default"});
        else
            rightArrow.css({"color" : "#419FDD", "cursor": "pointer"});;

        jQuery('#contributors-loading').css('display', 'none');
        jQuery('#contributors-loading-select').css('display', 'none');
    }


    function changeReport(page) {
        Cookie.set("reportType", jQuery("#contributors-report").val(), 365);

        jQuery.ajax({
            type:'GET',
            cache:false,
            url:'/sidebar/topContributors',
            data:'report=' + jQuery("#contributors-report").val() + '&page=' + page,
            dataType:'json',
            success:function(data) {
                if (data && data.entries.length > 0) {
                    _this.page = data.page;
                    _this.user = data.user;
                    _this.entries = data.entries;
                    _this.lastPage = data.lastPage;

                    createReport();
                } else {
                    var table = jQuery("#contributors-table");
                    table.html('');

                    table.append("<table class='table'><tbody><tr class='second'><td class='login'>Нет данных</td></tr></tbody></table>" +
                                 "<a href='/page/contribute' style='float:left; margin: 3px 0 0 5px;'>Получение Q</a>");

                    jQuery("#contribution-paging span:first").css({"color": "gray", "cursor": "default"});
                    jQuery("#contribution-paging span:last").css({"color": "gray", "cursor": "default"});

                    jQuery('#contributors-loading').css('display', 'none');
                    jQuery('#contributors-loading-select').css('display', 'none');
                }
            },
            error:function(xhr) {
                var status = 0;
                try { status = xhr.status; } catch(e) { };
                if (status >= 400  && status < 600) jQuery("#contributors-content").html("<span style='color:red'>Ошибка " + status + "</span>");
                jQuery("#contributors-content").html("<span style='color:red'>Ошибка соединения с сервером</span>");
            }
        });
    }

    function drawArrows() {
        var leftArrow = jQuery("#contribution-paging span:first");
        var rightArrow = jQuery("#contribution-paging span:last");

        leftArrow.click(function() {
            if (_this.page == 0) return false;
            else {
                jQuery('#contributors-loading').css('display', 'inline');
                changeReport(_this.page - 1);
            }
        });

        rightArrow.click(function() {
            if (_this.lastPage) return false;
            else {
                jQuery('#contributors-loading').css('display', 'inline');
                changeReport(_this.page + 1);
            }
        });
    }

    jQuery("#contributors-report").change(function() {
        jQuery('#contributors-loading-select').css('display', 'inline');
        changeReport(0);
    });
    changeReport(0);
    drawArrows();
}

// Invites

function InviteFriend() {
    if (Cookie.get("inviteFriendState") == "closed")
        jQuery('#invite-block .content').removeClass('opened').addClass("closed");

    jQuery("#invite-block .content form").submit(function() { return false; });

    jQuery("#invite-block .content .buttons input").click(function () {
        jQuery.ajax({
            type:'POST',
            cache:false,
            url: '/sidebar/inviteFriend',
            data: 'email=' + jQuery('#invite-block .content input[name=email]').val(),
            dataType: 'json',
            success: function(errors) {
                jQuery("#invite-block .content .errors").html("");
                jQuery("#invite-block .content .errors").removeClass("hidden");
                if (errors.length == 0) {
                    jQuery("#invite-block .content .errors").addClass("hidden");
                    jQuery("#invite-block .content form").before("<div class='thanks'><span>Приглашение было отправлено на указанный адрес<br/></span><a href='#'>пригласить еще</a></div>");
                    jQuery("#invite-block .content .thanks a").click(function () {
                        jQuery("#invite-block .content .thanks").remove();
                        jQuery("#invite-block .content form").slideDown();
                        return false;                        
                    });

                    jQuery("#invite-block .content form").slideUp();

                    jQuery('#invite-block .content form')[0].reset();
                } else {
                    for (var i = 0 ; i < errors.length ; i++) {
                        jQuery("#invite-block .content .errors").append("<div>" + errors[i] + "</div>");
                    }
                }
            },
            beforeSend: function() {
                jQuery("#invite-block .content .buttons img").css("display", "inline");
            },
            complete: function() {
                jQuery("#invite-block .content .buttons img").css("display", "none");
            },
            error: function(xhr) {
                var status = 0;
                try { status = xhr.status; } catch(e){};
                if (status == 403) jQuery("#invite-block .content form").html("<span style='color:red'>Вы не вошли в систему</span>");
                else {
                    if (status >= 400  && status < 600) jQuery("#invite-block .content form").html("<span style='color:red'>Ошибка " + status + "</span>");
                    else jQuery("#invite-block .content form").html("<span style='color:red'>Ошибка соединения с сервером</span>");
                }
            }
        });
        return false;
    });
}

//COOKIE

var Cookie = {
    set: function(name,value,expireDays) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + expireDays);
        document.cookie = name + "=" + value + ((expireDays == null) ? "" : ";expires=" + exdate.toGMTString()) + "; path=/";
    },
    get: function(name) {
        if (document.cookie.length > 0) {
            var start = document.cookie.indexOf(name + "=");

            if (start != -1) {
                start = start + name.length + 1;
                var end = document.cookie.indexOf(";", start);

                if (end == -1) end = document.cookie.length;
                return document.cookie.substring(start, end);
            }
        }
        return "";
    },
    remove: function(name) {
        document.cookie = name + "=" + ";path=/;expires=Thu, 01-Jan-1970 00:00:01 GMT";
    }
};