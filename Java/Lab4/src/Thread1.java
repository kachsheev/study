//Класс, реализирующий интерфейс Rannable
public class Thread1 implements Runnable
{
	Thread thr; 
	Num ThrNum;
	Thread1(Num num)
	{
		thr = new Thread(this, "noname");
		ThrNum = num;
	}
	
	public void start()
	{
		thr.start();
	}
	
	public void run()
	{
		while(ThrNum.GlobalCount!=0)
		{
			if(ThrNum.Thr)
			{
				ThrNum.Gen();
				ThrNum.GlobalCount--;
				ThrNum.Thr=false;
			}
		}
	}
}
