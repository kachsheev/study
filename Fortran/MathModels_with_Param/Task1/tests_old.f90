! ------------------------- Метод прогонки ------------------------- !
subroutine Test_Thomas(size1)
	
	integer size1, i
	real a(0:size1-1), b(0:size1-1), c(0:size1-1), f(0:size1-1), x(0:size1-1), f1(0:size1-1)
		!Таким извращенским образом 
		!мы можем задавать длину массивов
	
	!Инициализация массивов
	a(0) = 0.0; a(1) = 1.12;  a(2) = 2.24;  a(3) = 3.36;  a(4) = 4.48;  a(5) = 5.52
	b(0) = 1.12; b(1) = 2.24;  b(2) = 3.36;  b(3) = 4.48;  b(4) = 5.52;  b(5) = 0.0
	c(0) = 5.1; c(1) = 10.2; c(2) = 15.3; c(3) = 20.4; c(4) = 25.5; c(5) = 30.6
	f(0) = 5.61; f(1) = 10.52; f(2) = 15.43; f(3) = 20.34; f(4) = 25.25; f(5) = 30.16
	
	i=0
	print*, 'start A, B, C, F:'
	do while(i<size1)
		print*, a(i), c(i), b(i), f(i), i
		i = i +1 
	end do
	
	call Thomas_Algorithm(a,b,c,f,x,size1);

	print*, ""

	i=0;
	print*, "X:"
	do while(i<size1)
		print*, x(i), i; i = i + 1
	end do
	
	print*, ""
	i=0
	
	print*, 'finish F:'
	do while(i<size1)
		if(i==0)then
			f1(i) = x(i+1)*b(i)+x(i)*c(i)
		else 
			if(i==size1-1) then
				f1(i) = x(i-1)*a(i)+x(i)*c(i)
			else
				f1(i) = x(i-1)*a(i)+x(i+1)*b(i)+x(i)*c(i)
			end if
		end if
		
		print*, f1(i), i; i = i + 1
	end do
	print*, ""
	
	i=0
	print*, 'Невязка:'
	do while(i<size1)
		print*, f(i) - f1(i), i
		i = i+1
	end do
end subroutine Test_Thomas
! ------------------------------------------------------------------ !
! -------------------- Мой вариант задания -------------------- !
subroutine Test_VarS3(size1, big_r)
	integer size1, i
	real big_r, a(0:size1-1), b(0:size1-1), c(0:size1-1), f(0:size1-1), x(0:size1-1)
	real f1(0:size1-1)
	i=0;
	if(size1>2)then
		print*,'A, C, B, F, ind, |C|>|A|+|B|'
		do while(i<size1)
			
			
			call sub_koeff_a(float(i),size1,big_r,a(i))
			call sub_koeff_b(float(i),size1,big_r,b(i))
			call sub_koeff_c(float(i),size1,big_r,c(i))
			call sub_koeff_f(float(i),size1,big_r,f(i))
			
			print*, a(i),c(i),b(i),f(i), i, ( abs(c(i)) > abs(a(i))+abs(b(i)))
			i = i+1
		end do
		print*, ''
		
		call Thomas_Algorithm (a, b, c, f, x, size1)
		
		i=0
		print*, 'X:'
		do while(i<size1)
			print*, x(i)
			i = i+1
		end do
		print*, ''
		
		i=0
		print*, 'recalc F:'
		do while(i<size1)
			if(i==0)then
				f1(i) = x(i+1)*b(i)+x(i)*c(i)
			else	 
				if(i==size1-1) then
					f1(i) = x(i-1)*a(i)+x(i)*c(i)
				else
					f1(i) = x(i-1)*a(i)+x(i+1)*b(i)+x(i)*c(i)
				end if
			end if
		print*, f1(i), i; i = i + 1
		end do
		print*, ''
		
		i=0;
		print*, 'Невязка'
		do while(i<size1)
			print*, f(i)-f1(i), i
			i = i+1
		end do
		
	else
		print*, 'Error'
	end if
	
	
end subroutine Test_VarS3
! ------------------------------------------------------------- !
