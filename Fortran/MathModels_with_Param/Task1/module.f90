! -------------------- Метод прогонки -------------------- !
subroutine Thomas_Algorithm (a, b, c, f, x, size1)
	integer size1, i /2/
	real a(size1),b(size1),c(size1),f(size1), x(size1) 
	real alpha(2:size1), beta(2:size1)
	
	alpha(2) = -b(1)/c(1);	beta(2) = f(1)/c(1);
	
	i=2;
	do while(i<size1)
		alpha(i+1) = -b(i)/( a(i)*alpha(i) + c(i) )
		beta(i+1)  = ( f(i) - a(i)*beta(i) )/( a(i)*alpha(i) + c(i) )
		i = i+1
	end do
	
	x(size1) = (f(size1) - a(size1)*beta(size1))/(c(size1) + a(size1)*alpha(size1))
	
	i=size1-1
	do while(i>0)
		x(i) = alpha(i+1)*x(i+1)+beta(i+1)
		i = i-1
	end do

end subroutine Thomas_Algorithm
! -------------------------------------------------------- !
!
!
!
!
!
! --------------- Основные процедуры --------------- !
subroutine func_u(arg,res) ! считаем точные значения
	real arg, res
!	res = 1.0;
	res = (arg**3) - 2*(arg**2) + 1
end subroutine func_u;
! ----------------------------------------
subroutine func_k(arg,res) ! функция k
	real arg, res
!	res = 1.0;
	res = -arg*(arg - 2)/(3*arg - 4) ! arg <> 4/3
end subroutine
! ----------------------------------------
subroutine func_q(arg,res) ! функция q -- единичная
	real arg, res
	
	res = 1.0
end subroutine func_q
! ----------------------------------------
subroutine func_f(arg,res) ! функция f 
	real arg, res
!	res = 1.0
	res = (arg**3) + 3*(arg**2) - 8*(arg) +1
end subroutine func_f;
! -------------------------------------------------- !
!
!
!
!
!
! --------------- Считаем r, h, hbar --------------- !
subroutine func_to_r(ind, size1, big_r, res) ! тут точно все в порядке
	real ind, big_r, res
	integer size1
	
	res = big_r*ind/(size1-1)
end subroutine func_to_r
! ----------------------------------------
subroutine func_to_h(ind, size1, big_r, res)
	real ind, big_r, res
	integer size1
	
	real r0, r1
	
	call func_to_r(ind-1, size1, big_r, r0)
	call func_to_r(ind, size1, big_r, r1)
	
	res = r1 - r0
	
end subroutine func_to_h
! ----------------------------------------
subroutine func_to_hbar(ind, size1, big_r, res)
	real ind, big_r, res, res0
	integer size1

	if( int(ind) == 0 )then
		
		call func_to_h(ind+1, size1, big_r, res)
		
		res = res/2
	else
		if( int(ind) == size1-1 )then
			call func_to_h(ind, size1, big_r, res)
			res = res/2
		else
			call func_to_h(ind, size1, big_r, res)
			call func_to_h(ind+1, size1, big_r, res0)
			
			res = (res + res0)/2
		end if
	end if
	
end subroutine func_to_hbar
! -------------------------------------------------- !
!
!
!
!
!
! --------------- Считаем коэффициенты --------------- !
subroutine func_koef_a(ind, size1, big_r, res)
	real ind, big_r, res
	integer size1
	
	real r,k,h
	
	if( int(ind) == 0 )then
		res = 0.0
	else
		call func_to_r(ind-0.5, size1, big_r, r)
		call func_k(r,k)
		call func_to_h(ind, size1, big_r, h)
		
		res = - (r**2)*(k)/(h)
	end if
		
end subroutine func_koef_a
! ----------------------------------------
subroutine func_koef_b(ind, size1, big_r, res)
	real ind, big_r, res
	integer size1
	
	real r,k,h
	
	if( int(ind) == size1-1 )then
		res = 0.0
	else
		call func_to_r(ind+0.5, size1, big_r, r)
		call func_k(r,k)
		call func_to_h(ind+1, size1, big_r, h)
		
		res = - (r**2)*(k)/(h)
	end if
end subroutine func_koef_b
! ----------------------------------------
subroutine func_koef_c(ind, size1, big_r, res)
	real ind, big_r, res
	integer size1
	
	real r,k,h,q,hbar, chi /1.0/
	
	if( int(ind) == 0 )then
		call func_to_r(ind+0.5, size1, big_r, r)
		call func_k(r,k)
		call func_to_h(ind+1, size1, big_r, h)
		
		call func_to_hbar(ind, size1, big_r, hbar)
		call func_q(r,q)
		
		res = (r**2)*(k)/(h) + (hbar*(r**2)*q/3) 
		
	else ! для N
		if( int(ind) == size1-1 )then
			call func_to_r(ind, size1, big_r, r)
			res = (r**2)*chi
			
			call func_to_hbar(ind, size1, big_r, hbar)
			call func_q(r, q)
			
			res = res + hbar*(r**2)*q
			
			call func_to_r(ind-0.5, size1, big_r, r)
			call func_k(r,k)
			call func_to_h(ind, size1, big_r, h)
			
			res = res + (r**2)*k/h
		else ! общее
			call func_to_r(ind+0.5, size1, big_r, r)
			call func_k(r,k)
			call func_to_h(ind+1, size1, big_r, h)
			
			res = (r**2)*(k)/(h)
				
			call func_to_r(ind-0.5, size1, big_r, r)
			call func_k(r,k)
			call func_to_h(ind, size1, big_r, h)
			
			res = res + (r**2)*(k)/(h)
			
			call func_to_hbar(ind, size1, big_r, hbar)
			call func_to_r(ind, size1, big_r, r)
			call func_q(r,q)
			
			res = res + hbar*(r**2)*q
		end if
	end if
end subroutine func_koef_c
! ----------------------------------------
subroutine func_koef_f(ind, size1, big_r, res)
	real ind, big_r, res
	integer size1
	
	real r, f, hbar, chi /1.0/, nu /1.0/
	
	if( int(ind)==0 )then
		call func_to_hbar(ind, size1, big_r, hbar)
		call func_to_r(ind+0.5, size1, big_r, r)
		call func_f(r,f)
		
		res = 1/3*hbar*(r**2)*f
	else
		if( int(ind)==size1-1 )then
			call func_to_r(ind, size1, big_r, r)
			call func_f(r,f)
			call func_to_hbar(ind, size1, big_r, hbar)
			
			res = hbar*(r**2)*f + (r**2)*nu
			
		else
			call func_to_r(ind, size1, big_r, r)
			call func_f(r,f)
			call func_to_hbar(ind, size1, big_r, hbar)
			
			res = hbar*(r**2)*f
		end if
	end if
end subroutine func_koef_f
! ---------------------------------------------------- !