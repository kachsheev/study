! ------------------------- Метод прогонки ------------------------- !
subroutine Test_Thomas(size1)
	
	integer size1, i /0/
	real a(0:size1-1), b(0:size1-1), c(0:size1-1)
	real f(0:size1-1), v(0:size1-1), u(0:size1-1)
	real k(0:size1-1), r /0.0/, big_r /10.0/
	
	a(0)=0.0 ; b(0)=0.6 ; c(0)=4.0 ; f(0)=4.6 ;
	a(1)=0.1 ; b(1)=0.5 ; c(1)=5.0 ; f(1)=5.6 ;
	a(2)=0.2 ; b(2)=0.4 ; c(2)=6.0 ; f(2)=6.6 ;
	a(3)=0.3 ; b(3)=0.3 ; c(3)=7.0 ; f(3)=7.6 ;
	a(4)=0.4 ; b(4)=0.2 ; c(4)=8.0 ; f(4)=8.6 ;
	a(5)=0.5 ; b(5)=0.1 ; c(5)=9.0 ; f(5)=9.6 ;
	a(6)=0.6 ; b(6)=0.0 ; c(6)=10.0; f(6)=10.6;
	
	call Thomas_Algorithm(a,b,c,f,v,size1)
	
	do while(i<size1)
		print*, v(i)
		i = i + 1
	end do
	
end subroutine Test_Thomas
! ------------------------------------------------------------------ !
! -------------------- Мой вариант задания -------------------- !
subroutine Test_VarS3(size1, big_r)
	integer size1, i
	real big_r, a(0:size1-1), b(0:size1-1), c(0:size1-1)
	real f(0:size1-1), v(0:size1-1), u(0:size1-1)
	real f1(0:size1-1), r, max_delta
	
	i = 0
!	print*, 'a, c, b, f, r'
	do while(i<size1)
	
		call func_koef_a(float(i), size1, big_r, a(i))
		call func_koef_b(float(i), size1, big_r, b(i))
		call func_koef_c(float(i), size1, big_r, c(i))
		call func_koef_f(float(i), size1, big_r, f(i))
		
		call func_to_r(float(i), size1, big_r, r)
		call func_u(r,u(i))

!		print*, a(i), c(i), b(i), f(i), r
		
		i = i+1
	end do
	
	call Thomas_Algorithm(a,b,c,f,v,size1)
	print*,''
	
	max_delta = abs(u(0) - v(0))
	i = 1
	do while(i<size1)
		if(abs(u(i)-v(i))>max_delta)then
			max_delta = abs(u(i)-v(i))
		end if
		i = i+1
	end do
	
	print*,"u,              v,               max delta"
	i = 0
	do while(i<size1)
		if(i == 0)then
			print*,u(i),v(i), max_delta, size1-1
! 		else
! 			print*,u(i),v(i)
 		end if
		
		i = i + 1
	end do
	
end subroutine Test_VarS3
! ------------------------------------------------------------- !
