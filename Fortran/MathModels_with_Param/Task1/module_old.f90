! -------------------- Метод прогонки -------------------- !
subroutine Thomas_Algorithm (a, b, c, f, x, size1)
	integer size1, i /2/
	real a(size1),b(size1),c(size1),f(size1), x(size1) 
	real alpha(2:size1), beta(2:size1)

	!Прямой ход
	alpha(2) = - b(1)/c(1)
	beta(2) = f(1)/c(1)
	i=2
	do while(i < size1)
		alpha(i+1) = -b(i)/( a(i)*alpha(i) + c(i) ) 
		beta(i+1) = ( f(i) - a(i)*beta(i) )/( a(i)*alpha(i) + c(i) )
		i = i + 1
	end do
		
	!Обратных ход
	x(i) = ( f(i) - a(i)*beta(i) )/( c(i) + a(i)*alpha(i) )
	i=size1-1
	do while (i > 0)
		x(i) = alpha(i+1)*x(i+1)+beta(i+1)
		i = i - 1
	end do
end subroutine Thomas_Algorithm
! -------------------------------------------------------- !
! ---------- Считаем промежутки h, r, hbar --------- !
real function func_r(ind,size1,big_r) ! size1 - количесво точек
	real ind, res /0.0/
	integer size1
	real big_r
	
	if(ind < size1)then
		res = big_r/(size1-1)*ind
	else
		res = -1
	end if
	func_h = res
	
end function
! -------------------------------------------------- !
real function func_h(ind,size1,big_r) ! size1 - количесво точек
	real ind, res /0.0/
	integer size1
	real big_r
	
	if(ind < size1 .and. ind > 0)then
		res = func_r(ind,size1,big_r) - func_r(ind-1,size1,big_r)
	else
		res = func_r(ind+1.0,size1,big_r)
	end if
	
	func_h = res
end function
! -------------------------------------------------- !
real function func_hbar(ind,size1,big_r)
	real ind, res /0.0/
	integer size1
	real big_r
	
	if(int(ind)==0)then
		res = func_h(ind+1.0,size1,big_r)/2
	else
		if(int(ind)>0 .and. int(ind)<size1-1)then
			res = (func_h(ind,size1,big_r) + func_h(ind+1.0,size1,big_r))/2
		else
			res = func_h(ind,size1,big_r)
		end if
	end if
	func_hbar = res
end function
! -------------------------------------------------- !
! ---------- Считаем a,b,c,f --------- ! ! не знаю на счет f,k,q,chi
subroutine sub_koeff_a(ind,size1,big_r, koeff_a)
	real k /1.0/, ind, big_r, res /0.0/, koeff_a
	integer size1

	if(int(ind)==0)then
		koeff_a = 0.0;
		
	else 
		res = (func_r(ind-0.5,size1,big_r)**2*k)/func_h(ind,size1,big_r); !print*, res
		koeff_a = -res
	end if
	
end subroutine sub_koeff_a
! -------------------------------------------------------- !
subroutine sub_koeff_b(ind,size1,big_r,koeff_b)
	real k /1.0/, ind, big_r, res /0.0/, koeff_b
	integer size1
	real ind_p /0.0/

	if(int(ind)> -1 .and. int(ind)<size1-1)then
		ind_p = ind + 0.5
		res = (func_r(ind_p,size1,big_r)**2*k);! print*, res, func_h(ind,size1,big_r)
		koeff_b = -res/func_h(ind,size1,big_r); !print*, koeff_b
	else
		koeff_b = 0.0
	end if
end subroutine sub_koeff_b
! -------------------------------------------------------- !
subroutine sub_koeff_c(ind,size1,big_r,koeff_c)
	real k /1.0/, q /1.0/, chi /1.0/, ind, big_r, koeff_c
	integer size1
	real res /0.0/, ind_p /0.0/, ind_m /0.0/, a,b

	if(int(ind)==0)then
		ind_p = 0.5
		call sub_koeff_b(ind_p,size1,big_r,b);
		
		res = -b ! нужно уточнить
		res = res + (func_hbar(ind,size1,big_r)*(func_r(ind_p,size1,big_r)**2)*q/3)
	else
		ind_m = ind - 0.5
		ind_p = ind + 0.5
		if(int(ind)==size1-1)then
			call sub_koeff_a(ind_m,size1,big_r,a)
			
			res = (func_r(ind,size1,big_r)**2)*chi
			res = res - a
			res = res + func_hbar(ind,size1,big_r)*(func_r(ind,size1,big_r)**2)*q
		else
			call sub_koeff_a(ind_m,size1,big_r,a)
			call sub_koeff_b(ind_p,size1,big_r,b)
			
			res = -a -b ! нужно уточнить
			res = res + func_hbar(ind,size1,big_r)*(func_r(ind,size1,big_r)**2)*q
		end if
	end if
	koeff_c = res;
end subroutine sub_koeff_c
! -------------------------------------------------------- !
subroutine sub_koeff_f(ind,size1,big_r,koeff_f)
	real koeff_f, ind, big_r, f /1.0/, nu /1.0/
	integer size1
	
	if(int(ind) == 0)then
		koeff_f = func_hbar(ind,size1,big_r)*(func_r(ind+0.5,size1,big_r)**2)*f/3
	else
		if(int(ind) == size1-1)then
			koeff_f = func_hbar(ind,size1,big_r) * (func_r(ind,size1,big_r)**2)* f
			koeff_f = koeff_f + (func_r(ind,size1,big_r)**2)*nu
		else
			koeff_f = func_hbar(ind,size1,big_r) * (func_r(ind,size1,big_r)**2)* f
		end if
	end if
	
end subroutine
! -------------------------------------------------------- !
