{
��� �������� (����������) ������ � ����� � ���� ��� 
"����������� - ���������� �������":
(a n),  (a n-1) ...(a 0)
(b m),  (b m-1) ...(b 0)
��������� �������� ������, ��������� ��������� ������������ ������
���������� �� ����� ��������� R(X)=Pn(X)+Qm(X) � ����� �� ���� ��� � �����
}
program ex_four;

type
   m = ^st;
   st = record
      koef, pkz: integer;
      nx: m;
   end;

{�������� ������}
procedure read_in(f: text; var k_start: m);
var
   k: m; c: char;
begin
   new(k);
   k_start := k;
   read(f, c, k^.koef, k^.pkz, c, c);
   while (not eoln(f)) do
   begin
      new(k^.nx);
      k := k^.nx;
      read(f, c, k^.koef, k^.pkz, c, c);
   end;
   k := nil;
   readln(f);
end;

procedure proizvod(k_start: m);
var
   k: m;
begin
   k:=k_start;
   while k<>nil do
      begin
         k^.koef:=k^.koef*k^.pkz;
         if k^.pkz<>0 then
            k^.pkz:=k^.pkz-1
         else
            k^.pkz:=0;
         k:=k^.nx;
      end
end;

procedure sum_polynoms(var p_start: m; q_start: m);
var
   p, q: m;

begin
   p := p_start;
   while (p<>nil) do
   begin
      q:=q_start;
      repeat
         if q^.pkz=p^.pkz then
            p^.pkz:=p^.pkz+q^.pkz
          else
            q:=q^.nx;
      until (q=nil);
      p:=p^.nx;
   end;
end;

var
   fin: text;
   p_start, q_start, p, q: m;


begin
   assign(fin, 'f_in.txt');
   reset(fin);
   
   read_in(fin, p_start);
   read_in(fin, q_start);
   sum_polynoms(p_start, q_start);
   proizvod(p_start);
   
   p := p_start;
   
   repeat
      write('(', p^.koef);
      write(' ', p^.pkz, ')');
      p := p^.nx;
   until p = nil;
   close(fin);
end.