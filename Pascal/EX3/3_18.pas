  {� ��������� ����� F1 ����� ������ ������� 
  (�� ����� �� ��p���, �� ����� 15 ��������). 
  ���p������� �p�����p�:
  1. P1 - �op��p������ ����������������� ������ 
  � ��������� ���� string[15];
  2. P2 - ��p��p���� ��������� �� ��������, p������ 
  � ����p������ ��������� ������;
  3. P3 - ������ ����p������ ��������� ������ � ����� 
  �p���p�� �p���������� p��������� �p�����p P2 � P1.
  � ������� ���� �p�����p ����p��p����� �������� ����, 
  ������� ����p����� ����p��p�������� ������ � ��������� 
  ���� F2. ��������� ������ ����������.}
program ex_three;
type
m=^st;
st=record
     str:string[15];
     nx:m;
   end;

var
s_start{,dest}:m;
f1,f2:text;
n:integer;

procedure p1(var file_in:text; var lol_start:m; var n:integer);{������������ ������}
var lol:m;
begin
  n:=0;
  new(lol);
  lol_start:=lol;
  while (not eof(f1)) do
    begin
      new(lol^.nx);
      readln(f1,lol^.str);
      lol:=lol^.nx;
      n:=n+1;
    end;
    lol:=nil;
end;

procedure p2(var lolstart:m);{���������� �� ��������}
var
  c:string;
  lol:m;

begin
  lol := lolstart;
  repeat
    if (lol^.str > lol^.nx^.str)then
    begin
      c := lol^.str;
      lol^.str := lol^.nx^.str;
      lol^.nx^.str := c;
    end;
    lol := lol^.nx;
  until(lol^.nx = nil) 
end;

procedure p3(var file_out:text; lol_start:m);{����� ����������}
var lol:m;
begin
  lol:=lol_start;
  while (lol^.nx<>nil) do 
  begin
    writeln(file_out,lol^.str);
    lol:=lol^.nx;
  end;
end;

BEGIN
  new(s_start);
  assign(f1,'f1.txt');
  assign(f2,'f2.txt');
  reset(f1);
  rewrite(f2);
  p1(f1, s_start, n);
  while n>0 do
  begin
    p2(s_start);
    n:=n-1;
  end;
  p3(f2, s_start);
  close(f1);
  close(f2);
  write(n);
END.