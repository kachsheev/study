program lol_76;

type
  m = array[1..10] of string[25];

var
  s: m;
  x: integer;
  fin, fout: text;
  ch: char;

{���������, ����������� ��� ��������� ������}
procedure sort(var lol: m; k: integer);
var
  c: string;
  x: integer;

begin
  repeat
    for x := 1 to k do
      if s[x] > s[x + 1] then
      begin
        c := s[x];
        s[x] := s[x + 1];
        s[x + 1] := c;
      end;
    k := k - 1;
  until k = 0;
end;

{����������� ���������..����� � �������� ����}
procedure vivod(var fout: text; var s: m; d: integer);
begin
  if d >= 1 then
  begin
    writeln(fout, s[11 - d]);
    vivod(fout, s, d-1);
  end
  else writeln(fout);
end;

{}
procedure vvod(var fin: text; var s: m; x:integer);
begin
  if x>=1 then
  begin
    repeat
      read(fin, ch);
      s[11-x] := s[11-x] + ch;
    until (Eoln(fin));
    readln(fin);
    vvod(fin,s,x-1);
  end;
end;

begin
  assign(fin, 'fin.txt');
  reset(fin);
  assign(fout, 'fout.txt');
  rewrite(fout);
  
  {���������� � ������ ���� �������� �������}
  vvod(fin,s,10);
  
  {������ � �������� ���� �������� ������}
  vivod(fout, s, 10);
  
  {�������� ���� ��������� ����������}
  sort(s, 9);
  
  {������� ��������� ����� ������}
  vivod(fout, s, 10);

  close(fin);
  close(fout);
end.