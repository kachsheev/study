{
�� ������� ����� ������ ���������� ����� ��������������� ���������, 
����������  ��������������  ����� (������� ��������� �����) 
�������������� ���������, ������� �������� +,-,*,/  ��� ������� ��������.
����������� ���������:
   1.P1-�������� ������������ ������ ���������� ����� ��������������� ���������, 
     ��������� ��������;
   2.P2-�������������� ���������� ����� � �����������.
� ������ ����������� �������� ���������� ����� ������ ��������� �� ������.
}
program ex_five;

type
    tr = ^tree;
    tree = record
        lv: tr;
        rv: tr;
        ch: char;
    end;

{������������ ������}
procedure form_tree(f: text; var p: tr);
    procedure tree_end(p: tr);{����������� ����� ����� � ������}
    begin
        new(p^.lv);
        new(p^.rv);
        p^.lv := nil;
        p^.rv := nil;
    end;

var
    c: char;
begin
   	{��� ����� �����}
    if not eoln(f) then
    begin
        read(f, c);
        write(c);
        new(p^.lv);
        p^.lv^.ch := c;
        if c in ['+', '-', '/', '*'] then
            form_tree(f, p^.lv)
      		else
        if c in ['A'..'Z'] then
            tree_end(p^.lv);
    end;
    
   	{��� ������ �����}
    if not eoln(f) then
    begin
        read(f, c);
        write(c);
        new(p^.rv);
        p^.rv^.ch := c;
        if c in ['+', '-', '/', '*'] then
            form_tree(f, p^.rv)
      		else
        if c in ['A'..'Z'] then
            tree_end(p^.rv);
    end;
end;


{------------------------------------------------------------------------------}
{������� � ����������� �����}
procedure p2(p: tr);
begin
    if not ((p = nil)) then
    begin
        p2(p^.lv);
        p2(p^.rv);
        write(p^.ch);
    end;
end;
{=================================================================}

{�������� �� ������������}
procedure p1(f: text; var k: integer);
var
    c: char;
begin
    if not (eoln(f)) then
    begin
        read(f, c);
        if c in ['A'..'Z'] then
        begin
            k := k + 1;
            p1(f, k);
        end   
        else if c in ['+', '-', '*', '/'] then
        begin
            k := k - 1;
            p1(f, k);
        end
        else
            write('������� ��������� ���������� �������')
    end;
    if k <> 1 then
        write('������� ��������� ���������� �������')
end;

var
    p, tree_start: tr;
    f: text;
    c: char;
    k: integer;

begin
    assign(f, 'file.txt');
    reset(f);
    p1(f, k);
    if (k = 1) then
    begin
        reset(f);
        new(tree_start);
        p := tree_start;
        read(f, c);
        write(c);
        if c in ['+', '-', '*', '/'] then
        begin
            p^.ch := c;
            form_tree(f, p);
            writeln('');
            p := tree_start;
            p2(p);
        end;
    end;   
    close(f);
    writeln();
end.